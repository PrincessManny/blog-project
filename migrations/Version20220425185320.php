<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220425185320 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F2261B4C3');
        $this->addSql('DROP INDEX IDX_B6BD307F2261B4C3 ON message');
        $this->addSql('ALTER TABLE message ADD addressee VARCHAR(255) NOT NULL, DROP addressee_id, CHANGE sender_id sender_id INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE message ADD addressee_id INT NOT NULL, DROP addressee, CHANGE sender_id sender_id INT NOT NULL');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F2261B4C3 FOREIGN KEY (addressee_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_B6BD307F2261B4C3 ON message (addressee_id)');
    }
}
