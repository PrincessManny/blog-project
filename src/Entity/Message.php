<?php

namespace App\Entity;

use App\Repository\MessageRepository;
use Doctrine\ORM\Mapping as ORM;


#[ORM\Entity(repositoryClass: MessageRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Message
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\PrePersist]
    public function setCreatedAtValue()
    {
        $this->date = new \DateTimeImmutable();
    }

    //#[ORM\ManyToOne(targetEntity: User::class)]
    //#[ORM\JoinColumn(nullable: false)]
    #[ORM\Column(type: 'string', length: 255)]
    private $sender;

    //#[ORM\ManyToOne(targetEntity: User::class)]
    //#[ORM\JoinColumn(nullable: false)]
    #[ORM\Column(type: 'string', length: 255)]
    private $addressee;

    #[ORM\Column(type: 'datetime_immutable')]
    private $date;

    #[ORM\Column(type: 'text')]
    private $message;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSender(): ?string
    {
        return $this->sender;
    }

    public function setSender(?string $sender): self
    {
        $this->sender = $sender;

        return $this;
    }

    public function getAddressee(): ?string
    {
        return $this->addressee;
    }

    public function setAddressee(?string $addressee): self
    {
        $this->addressee = $addressee;

        return $this;
    }

    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(\DateTimeImmutable $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
