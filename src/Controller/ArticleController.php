<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Article;
use App\Entity\User;
use App\Form\CommentFormType;
use App\Repository\CommentRepository;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use App\ImageOptimizer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class ArticleController extends AbstractController
{
    private $twig;
    private $entityManager;
    private $imageOptimizer;

    public function __construct(Environment $twig, EntityManagerInterface $entityManager, ImageOptimizer $imageOptimizer)
    {
	$this->twig = $twig;
	$this->entityManager = $entityManager;
	$this->imageOptimizer = $imageOptimizer;
    }

    #[Route('/article', name: 'homepage')]
    public function index(articleRepository $articleRepository): Response
    {
	return new Response($this->twig->render('article/index.html.twig', [
            'articles' => $articleRepository->findAll(),
        ]));
    }


    #[Route('/article/{slug}', name: 'article')]
    public function show(Request $request, Article $article, CommentRepository $commentRepository, UserRepository $UserRepository, string $photoDir): Response
    {
	$comment = new Comment();
	$form = $this->createForm(CommentFormType::class, $comment);
	$form->handleRequest($request);

	if ($form->isSubmitted() && $form->isValid()) {
	    $comment->setArticle($article);

            if ($photo = $form['photo']->getData()) {
                $filename = bin2hex(random_bytes(6)).'.'.$photo->guessExtension();
                try {
                    $photo->move($photoDir, $filename);
		    $this->imageOptimizer->resize($photoDir.'/'.$filename);
                } catch (FileException $e) {
              }
                $comment->setPhotoFilename($filename);
            }

	    $this->entityManager->persist($comment);
	    $this->entityManager->flush();

	    return $this->redirectToRoute('article', ['slug' => $article->getSlug()]);
	}

	$offset = max(0, $request->query->getInt('offset', 0));
	$paginator = $commentRepository->getCommentPaginator($article, $offset);

	return new Response($this->twig->render('article/show.html.twig', [
	    'comments' => $paginator,
	    'article' => $article,
	    'previous' => $offset - CommentRepository::PAGINATOR_PER_PAGE,
	    'next' => min(count($paginator), $offset + CommentRepository::PAGINATOR_PER_PAGE),
	    'comment_form' => $form->createView(),
	]));
    }
}

