<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Article;
use App\Entity\User;
use App\Form\CommentFormType;
use App\Repository\CommentRepository;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class UserController extends AbstractController
{
    private $twig;
    private $entityManager;

    public function __construct(Environment $twig, EntityManagerInterface $entityManager)
    {
	$this->twig = $twig;
	$this->entityManager = $entityManager;
    }

    #[Route('/profile/{id}', name: 'profile')]
    public function show(Request $request, UserRepository $userRepository,CommentRepository $commentRepository, string $id): Response
    {
	$user = $userRepository->find($id);
	return new Response($this->twig->render('user/show.html.twig', [
	    'user' => $user,
	    'comments' =>$commentRepository->findBy(['user' => $user]),
	]));
    }
}

