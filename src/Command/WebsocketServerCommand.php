<?php
namespace App\Command;

use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use App\Websocket\MessageHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Persistence\ManagerRegistry;

class WebsocketServerCommand extends Command
{
    protected static $defaultName = "run:websocket-server";
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;

	parent::__construct();
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
    $port = 5001;
    $output->writeln("Starting server on port " . $port);
    $server = IoServer::factory(
        new HttpServer(
            new WsServer(
                new MessageHandler($this->doctrine)
            )
        ),
        $port
    );
    $server->run();
    return 0;
    }
}
