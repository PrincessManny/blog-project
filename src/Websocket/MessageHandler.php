<?php
namespace App\Websocket;

use Exception;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use SplObjectStorage;
use App\Entity\Message;
use Doctrine\Persistence\ManagerRegistry;

class MessageHandler implements MessageComponentInterface
{
    protected $connections;
    private $em;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->connections = new SplObjectStorage;
        $this->em = $doctrine->getManager();
    }

    public function onOpen(ConnectionInterface $conn)
    {
	$this->connections->attach($conn);
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
	foreach($this->connections as $connection)
	{
	    if($connection === $from)
		{
		    continue;
		}
	    $connection->send($msg);
	}
	$message = new Message();
	$message->setSender('sender');
	$message->setAddressee('addressee');
	$message->setMessage(json_decode($msg, true)['message']);

	$this->em->persist($message);
	$this->em->flush();
    }

    public function onClose(ConnectionInterface $conn)
    {
	$this->connections->detach($conn);
    }

    public function onError(ConnectionInterface $conn, Exception $e)
    {
	var_dump($e);
	$this->connections->detach($conn);
	$conn->close();
    }
}
