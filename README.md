# 🎉 Maya's Box 🎉

## *Top anecdote gold collection book*

Maya's Box is a cloud-enabled, mobile-ready, Symfony powered CRUD web-application (and chat!!!)

 - Read and comment best anecdotes ever
 - Chat with your friends
 - Upload reaction images
 - ✨Sheeeeeesh✨

# Features
- Add your favourite anecdotes (but only if you're an admin 😈)
- Comment anecdotes 📩
- Attach images to your comments 👀
- Chat with your frens 🐸
- View comments history of your crush 💘

An anecdote is always presented as the recounting of a real incident involving actual people and usually in an identifiable place, but here's a couple of jokes:
> I got fired from the calendar factory, **just for taking a day off**.

> Care to seduce a large woman? **Piece of cake**.

# Installation
## Requirements
  - **Symfony** 5.4
  - **PHP** 8.0
  - **NodeJS** 16.14
  - **MySQL** 5.7

Install symfony dependencies via composer
```
composer install
```

Make migrations with Doctrine
```
php bin/console doctrine:migrations:migrate
```
Add DB connection string to .env file.

Build frontend with Yarn
```
yarn dev
```

Add entrypoint to your http server of choice

    public/index.php


#### Start Websocket server

```
php bin/console run:websocket-server
```


# Development

Want to contribute? Great! - start by sending me an anecdote or a joke via tg: 🌸 @PrincessManny 🌸
